#!/bin/sh

mkdir zeroc-ice-manual-$2
cp $3 zeroc-ice-manual-$2
tar cf ../zeroc-ice-manual_$2.orig.tar zeroc-ice-manual-$2
xz ../zeroc-ice-manual_$2.orig.tar
rm -rf zeroc-ice-manual-$2
exit 0
